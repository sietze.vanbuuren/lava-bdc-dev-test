#/bin/sh
release=v2021
version=v2021.0
release_version=v2021
variant=minimal
arch=amd64
visibility=personal
priority=high
board=uefi
image_date=20210805
#baseurl="https://apertispro-dev:crJtdDGtWSWqE94z@images-apertispro.boschdevcloud.com/apertispro"
baseurl="https://images.apertis.org"
imgpath="release/$release/$version/$arch/$variant"
#image_name=apertispro_${release}-${variant}-${arch}-${board}_${version}
image_name=apertis_${release}-${variant}-${arch}-${board}_${version}
profile_name=qemu-minimal-amd64


lqa submit -g lava/profiles.yaml \
  -p ${profile_name} \
  -t priority:${priority} \
  -t imgpath:${imgpath} \
  -t arch:${arch} \
  -t release_version:${release_version} \
  -t release:${release} \
  -t visibility:${visibility} \
  -t image_date:${version} \
  -t baseurl:${baseurl} \
  -t image_date:${image_date} \
  -t image_name:${image_name} # -n
